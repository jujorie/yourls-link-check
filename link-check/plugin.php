<?php
/*
Plugin Name: Link Check
Plugin URI: https://gitlab.com/jujorie/yourls-link-check
Description: Verify if a url is alive on the admin dashboard
Version: 0.1.6
Author: Juan Jose Riera Esteban
Author URI: https://gitlab.com/jujorie
*/

// No direct call
if( !defined( 'YOURLS_ABSPATH' ) ) die();

// Signature to make API calls. must be defined in <root>/user/config.php
// using the value of signature key found in the Tools menu
if( !defined( 'HCK_KEY' ) ) die();

// Define the constant that indicates how deep must check for redirection.
// defaults to 10
if(!defined('HCK_REDIRECTION_COUNT')) {
    define('HCK_REDIRECTION_COUNT', 10);
}

/**
 * Test a URL to verify
 */
function hck_check_url($url, $count) {
    if(!$count) {
        return array(false, "400");
    }
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    $http_respond = trim(strip_tags(curl_exec($ch)));
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $last_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
    curl_close($ch);

    if($http_code == "302") {
        return hck_check_url($last_url, $count - 1);
    } else if ($http_code == "200") {
        return array(true, "$http_code");
    }
    return array(false, "405");
}


yourls_add_filter('api_action_is_active', 'hck_is_active');
function hck_is_active() {
    if( !isset( $_REQUEST['url'] ) ) {
		return array(
			'statusCode' => 400,
			'simple'     => "Need a 'url' parameter",
			'message'    => 'error: missing param',
		);
	}

    list($status, $status_code) = hck_check_url($_REQUEST['url'], HCK_REDIRECTION_COUNT);
    if($status == true) {
        $return = array(
            'errorCode' => 200,
            'message'   => 'Url Ok',
            'simple'    => 'Url Ok',
        );
    } else {
        $return = array(
            'errorCode' => $status_code,
            'message'   => 'Url is unreachable',
            'simple'    => 'Not available',
        );
    }

    return $return;
}

yourls_add_filter('table_add_row_action_array', 'hck_add_button' );
function hck_add_button( $actions, $short_url ) {
    $table_url = YOURLS_DB_TABLE_URL;
    $short_url  = yourls_sanitize_keyword( $short_url );
    $res = yourls_get_db()->fetchObject("SELECT url FROM `$table_url` WHERE `keyword` = :keyword", array('keyword' => $short_url));

    $id = yourls_unique_element_id();
    $actions["health"] = array(
        'href'    => "$res->url",
        'id'      => "health-button-$id",
        'title'   => yourls_esc_attr__( "health" ),
        'anchor'  => yourls__( 'health' ),
    );

    return $actions;
}

yourls_add_action( 'html_head', 'hck_add_styles_and_scripts' );
function hck_add_styles_and_scripts() { ?>
<style type="text/css">
    td.actions .button.button_health {
        background-color: yellow;
        position: relative;
        visibility: visible;
    }

    td.actions .button.button_health.ready {
        background-color: green;
    }

    td.actions .button.button_health.broken {
        background-color: red;
    }
</style>
<script type="application/javascript">
    $(()=>{
        async function checkUrl(url) {
            return new Promise(resolve => {
                $.ajax({
                    type: "POST",
                    url: `/yourls-api.php?signature=<?php echo HCK_KEY ?>`,
                    data: {
                        url,
                        action: "is_active",
                        format: "json"
                    }
                })
                .then(()=>{
                    resolve(true)
                })
                .catch(()=>{
                    resolve(false)
                })
            })
        }

        async function checkAnchor() {
            const $this = $(this);
            const url = $this.attr("href")
            if($this.hasClass("ready")) $this.removeClass("ready")
            if($this.hasClass("broken")) $this.removeClass("broken")
            const result = await checkUrl(url);
            if(result) {
                $this.addClass("ready");
            } else {
                $this.addClass("broken");
            }
        }

        $(".button_health")
            .click(function onClick(e){
                e.preventDefault();
                checkAnchor.call(this);
            })
            .each(checkAnchor)
    })
</script>
<?php }
