# YOURLS-Link-Check

![link-check](./assets/link-check.png)

Verify if all the __urls__ on the **YOURLS** admin dashboard are ok. It use deep check to verify all redirection.

## Requirements
* A working [YOURLS](https://github.com/YOURLS/YOURLS) installation
* php-curl installed and activated

## Installation

1. Place the __link-check__ folder in _YOURLS/user/plugins/_

2. Add the signature parameter in _YOURLS/user/config.php_
```php
//
// In YOURLS/user/config.php
//
define('HCK_KEY', 'placeholder');
```

* **Warn**: you must change the _placeholder_ with the **Secret Signature Token** found on the _tools_ menu in the admin page

![sample](./assets/signature-token.png)

3. Activate in the Admin interface

## License

[MIT](LICENSE)